package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

var (
	start     = "/Users/yurko/Google Drive"
	scanCount = 0
)

type Dir struct {
	Path      string
	Childrens map[string]*Dir
}

func main() {
	entrypoint := Dir{
		Path: start,
	}
	scan(&entrypoint)

	enc := json.NewEncoder(os.Stdout)
	enc.Encode(entrypoint)
}

func scan(dir *Dir) {
	res, _ := ioutil.ReadDir(dir.Path)

	if len(res) > 0 {
		dir.Childrens = make(map[string]*Dir)
	}

	for _, item := range res {
		if item.IsDir() && !strings.HasPrefix(item.Name(), ".") {
			children := Dir{
				Path: dir.Path + "/" + item.Name(),
			}

			dir.Childrens[item.Name()] = &children
		}
	}

	for _, children := range dir.Childrens {
		scan(children)
	}
}
