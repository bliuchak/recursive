package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

type Dir struct {
	Path      string
	Childrens map[string]Dir
	Depth     int
}

func main() {
	startFolder := "Google Drive"
	start := "/Users/yurko/" + startFolder
	res, err := ioutil.ReadDir(start)
	if err != nil {
		panic(err)
	}

	data := make(map[string]Dir)

	childrens := make(map[string]Dir)
	for _, dir := range res {
		if dir.IsDir() && !strings.HasPrefix(dir.Name(), ".") {
			childrens[dir.Name()] = scan(start+"/"+dir.Name(), 0)
		}
	}

	data = map[string]Dir{
		startFolder: Dir{
			Path:      start,
			Childrens: childrens,
		},
	}

	enc := json.NewEncoder(os.Stdout)
	enc.Encode(data)
}

func scan(path string, depth int) Dir {
	depth++

	res, err := ioutil.ReadDir(path)
	if err != nil {
		panic(err)
	}
	var childrens map[string]Dir

	if len(res) > 0 {
		childrens = make(map[string]Dir)
		for _, item := range res {
			if item.IsDir() && !strings.HasPrefix(item.Name(), ".") {
				childrens[item.Name()] = scan(path+"/"+item.Name(), depth)
			}
		}
	}

	if len(childrens) == 0 {
		childrens = nil
	}

	dir := Dir{
		Path:      path,
		Childrens: childrens,
		Depth:     depth,
	}

	return dir
}
